<!DOCTYPE html>
<head>
  <meta charset="UTF-8">
  <title>Información de libro</title>
  <link rel="stylesheet" type="text/css" href="../css/estilo.css" />
</head>
<body>
<?php
  $clave_ejemplar = $_GET['clave_ejemplar'];

  if (empty($clave_ejemplar)) {
?>
  <p>Error, no se ha indicado la clave del ejemplar</p>
<?php
  } else {
    $servidorbd = "localhost";
    $nombrebd = "prueba";
    $usuariobd= "programador";
    $contraseniabd = "12345";

    $dbconn = pg_connect("host=$servidorbd dbname=$nombrebd user=$usuariobd password=$contraseniabd")
    or die('No se ha podido conectar: ' . pg_last_error());

    $query = "select clave_ejemplar, conservacion_ejemplar, isbn
      from biblioteca.ejemplar
      where clave_ejemplar = '".$clave_ejemplar."';";

    $ejemplar = pg_query($query) or die('La consulta falló: ' . pg_last_error());

    if (pg_num_rows($ejemplar) == 0) {
?>
  <p>No se ha encontrado algún ejemplar con clave <?php echo $clave_ejemplar; ?></p>
<?php
    } else {
      $tupla = pg_fetch_array($ejemplar, null, PGSQL_ASSOC);
      $conservacion_ejemplar = $tupla['conservacion_ejemplar'];
?>
<table>
  <caption>Información de Ejemplar</caption>
  <tbody>
    <tr>
      <th>CLAVE</th>
      <td><?php echo $clave_ejemplar; ?></td>
    </tr>
    <tr>
      <th>CONSERVACION</th>
      <td><?php echo $conservacion_ejemplar; ?></td>
    </tr>
    <tr>
      <th>Libro/os</th>
      <td>
<?php
      $query = "select titulo_libro
        from biblioteca.libro as LA
        inner join biblioteca.ejemplar as A
          on (LA.isbn = A.isbn and LA.isbn = '".$isbn."');";

      $libros = pg_query($query) or die('La consulta falló: ' . pg_last_error());
      if (pg_num_rows($libros) == 0) {
?>
        <p>Sin libros</p>
<?php
      } else {
?>
        <ul>
<?php
        while ($tupla = pg_fetch_array($autores, null, PGSQL_ASSOC)) {
          foreach ($tupla as $atributo) {
?>
          <li><?php echo $atributo; ?></li> 
<?php
          }
        }
?>
        </ul>
<?php
      }
?>
    </tr>
    <tr>
      <th>Autor/es</th>
      <td>
<?php
      $query = "select nombre_autor
        from biblioteca.libro_autor as LA
        inner join biblioteca.autor as A
          on (LA.id_autor = A.id_autor and LA.isbn = '".$isbn."');";

      $autores = pg_query($query) or die('La consulta falló: ' . pg_last_error());
      if (pg_num_rows($autores) == 0) {
?>
        <p>Sin autores</p>
<?php
      } else {
?>
        <ul>
<?php
        while ($tupla = pg_fetch_array($autores, null, PGSQL_ASSOC)) {
          $id_autor = $tupla['id_autor'];
          $nombre_autor = $tupla['nombre_autor'];
?>
          <li><?php echo $id_autor." / ".$nombre_autor; ?></li> 
<?php
        }
?>
        </ul>
<?php
      }
    }
  }
?>
    </tr>
  </tbody>
</table>

<?php
  pg_free_result($result);
  pg_close($dbconn);
?>

<ul>
  <li><a href="../inicio.html">Regresar al inicio</a></li>
  <li><a href="ejemplares.php">Lista de libros</a></li>
</ul>

</body>
</html>