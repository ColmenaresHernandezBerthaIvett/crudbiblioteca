<!DOCTYPE html>
<head>
  <meta charset="UTF-8">
  <title>Formulario de Ejemplar</title>
  <link rel="stylesheet" type="text/css" href="../css/estilo.css" />
</head>
<body>

<form action="create-ejemplar.php" method="post">
<table>
  <caption>Información de Ejemplar</caption>
  <tbody>
    <tr>
      <th>CLAVE</th>
      <td><input type="text" name="clave_ejemplar" /></td>
    </tr>
    <tr>
      <th>CONSERVACION/th>
      <td><input type="text" name="conservacion_ejemplar"/></td>
    </tr>
    <tr>
      <th>ISBN</th>
      <td><input type="text" name="isbn"/></td>
    </tr>
  </tbody>
</table>
<input type="submit" name="submit" value="CREATE" />
</form>

<ul>
  <li><a href="../inicio.html">Regresar al inicio</a></li>
</ul>

</body>
</html>